let sortKeys = ['name', 'identification', 'displayOrder'];

function t(o: any, key: string): any
{
    if(Array.isArray(o[key]))
    {
        let arr = o[key] as any[];
        if(arr.length > 0)
        {
            arr.sort((a,b) => {
                let sortKey = Object.keys(a).find(prop => sortKeys.includes(prop));
                if(sortKey === undefined)
                {
                    return 0;
                }
                return a[sortKey] < b[sortKey] ? -1 : a[sortKey] > b[sortKey] ? 1 : 0;
            });
        }
        for(var obj in arr)
        {
            if(obj === Object(obj))
            {
                Object.keys(obj).forEach(key => t(obj, key));
            }
        }
    }
}

export function transform(input: string): string
{
    let obj = JSON.parse(input);
    Object.keys(obj).forEach(key => t(obj, key));
    return JSON.stringify(obj, null, 2);
}